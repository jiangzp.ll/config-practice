package com.twuc.configpractice.repo;

import com.twuc.configpractice.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffRepo extends JpaRepository<Staff, Long> {

}

