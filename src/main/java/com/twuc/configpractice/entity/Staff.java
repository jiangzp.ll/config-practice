package com.twuc.configpractice.entity;


import javax.persistence.*;

@Entity
@Table(name = "staff")
public class Staff {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private Long officeId;

    public Staff() {
    }

    public Staff(String firstName, String lastName, Long officeId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.officeId = officeId;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getOfficeId() {
        return officeId;
    }
}
